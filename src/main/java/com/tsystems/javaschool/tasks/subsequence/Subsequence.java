package main.java.com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {


    public boolean find(List x, List y) {
        if (x == null) {
            throw new IllegalArgumentException();
        }
        if (y == null) {
            throw new IllegalArgumentException();
        }
        List<Object> mainList = new ArrayList<>(x);
        List<Object> dependentList = new ArrayList<>(y);
        int index = 0;
        boolean find = true;
        for (Object value : mainList) {
            if (dependentList.indexOf(value) >= index) {
                index = dependentList.indexOf(value);
                find = true;
            } else {
                find = false;
                break;
            }
        }
        return find;
    }
}
