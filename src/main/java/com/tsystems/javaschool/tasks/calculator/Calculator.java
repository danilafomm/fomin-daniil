package main.java.com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {
    public String evaluate(String statement) {
        if (statement == null || statement.indexOf(",") > 0 || statement.indexOf("//") > 0) {
            return null;
        }
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        Object result = null;
        try {
            result = engine.eval(statement);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        if (result == null) {
            return null;
        } else if (result.toString() == "Infinity") {
            return null;
        } else {
            return result.toString();
        }
    }
}
