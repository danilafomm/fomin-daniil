package main.java.com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.floor;

public class PyramidBuilder {
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size() > 10000) {
            throw new CannotBuildPyramidException("i cant do it, so many values");
        }
        List<Integer> inptnmb = new ArrayList<>(inputNumbers);
        if (inptnmb.contains(null)) {
            throw new CannotBuildPyramidException("there's some null value");
        }

        Collections.sort(inptnmb);
        int arrayHeight = heightSearcher(inptnmb.size());
        int arrayWidth = arrayHeight * 2 - 1;
        int[][] pyramid = new int[arrayHeight][arrayWidth];
        int counterForList = 0;
        int centerPosition = (int) (floor((arrayWidth - 1) / 2));
        for (int i = 0; i < arrayHeight; i++) {
            int actualPosition = centerPosition - i;
            for (int c = 0; c < i + 1; c++) {
                pyramid[i][actualPosition] = inptnmb.get(counterForList);
                actualPosition = actualPosition + 2;
                counterForList++;
            }
        }
        return pyramid;
    }


    public int heightSearcher(int size) {
        int hightArray = 1;
        int summ = 1;
        do {
            hightArray++;
            summ = summ + hightArray;
            if (summ > size) {
                throw new CannotBuildPyramidException("not symmetrical pyramid");
            }
        } while (summ != size);
        return hightArray;
    }
}
